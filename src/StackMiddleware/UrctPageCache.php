<?php

namespace Drupal\urct\StackMiddleware;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\PageCache\RequestPolicyInterface;
use Drupal\Core\PageCache\ResponsePolicyInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Drupal\page_cache\StackMiddleware\PageCache;

/**
 * Overrides the core page cache handling middleware to set different cache for crawlers.
 */
class UrctPageCache extends PageCache {


  /**
   * {@inheritdoc}
   */
  protected function getCacheId(Request $request) {
    // Once a cache ID is determined for the request, reuse it for the duration
    // of the request. This ensures that when the cache is written, it is only
    // keyed on request data that was available when it was read. For example,
    // the request format might be NULL during cache lookup and then set during
    // routing, in which case we want to key on NULL during writing, since that
    // will be the value during lookups for subsequent requests.
    if (!isset($this->cid)) {
      $cid_parts = [
        $request->getSchemeAndHttpHost() . $request->getRequestUri(),
        $request->getRequestFormat(NULL),
      ];
      if (\Drupal::service('urct.referral_manager')->isCrawler($request)) {
        $cid_parts[] = 'crawler';
      }
      // $service = $this->container->get('urct.referral_manager');
      $this->cid = implode(':', $cid_parts);
    }
    return $this->cid;
  }

}
